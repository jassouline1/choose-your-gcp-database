import streamlit as st
import vertexai
from vertexai.generative_models import GenerativeModel
from google.cloud import bigquery
import json

# Configuration GCP
PROJECT_ID = 'genai-use-cases'
LOCATION = 'us-central1'
DATASET_ID = 'choose_your_database_dataset'
TABLE_ID = 'feedback_table'

# Initialisation Vertex AI
vertexai.init(project=PROJECT_ID, location=LOCATION)

generation_config = {
    "temperature": 0.2,
}
safety_settings = {
}

# Initialisation BigQuery Client
client = bigquery.Client(project=PROJECT_ID)

def create_dataset_if_not_exists(dataset_id):
    dataset_ref = client.dataset(dataset_id)
    try:
        client.get_dataset(dataset_ref)
        #st.write(f"Dataset {dataset_id} already exists.")
    except:
        #st.write(f"Creating dataset {dataset_id}.")
        dataset = bigquery.Dataset(dataset_ref)
        dataset.location = LOCATION
        client.create_dataset(dataset)

def create_table_if_not_exists(dataset_id, table_id):
    table_ref = client.dataset(dataset_id).table(table_id)
    try:
        client.get_table(table_ref)
        #st.write(f"Table {table_id} already exists.")
    except:
        #st.write(f"Creating table {table_id}.")
        schema = [
            bigquery.SchemaField("user_inputs", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("response", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("feedback", "STRING", mode="REQUIRED"),
            bigquery.SchemaField("reason", "STRING", mode="NULLABLE"),
        ]
        table = bigquery.Table(table_ref, schema=schema)
        client.create_table(table)

# Check and create dataset and table if not exists
create_dataset_if_not_exists(DATASET_ID)
create_table_if_not_exists(DATASET_ID, TABLE_ID)

# Define the questions and options
st.title("GCP Database Solution Selector")

st.header("1. Data Type")

data_format = st.radio(
    "What is the primary format of your data?",
    ('Structured (tables, relations)', 'Unstructured (files, images, videos)', 'Semi-structured (JSON, XML)', "I don't know")
)

data_volume = st.radio(
    "What is the approximate size of your data?",
    ('Less than 1 GB', 'Between 1 GB and 1 TB', 'Between 1 TB and 1 PB', 'More than 1 PB', "I don't know")
)

data_growth = st.radio(
    "Do you expect significant growth in your data over the next 12 months?",
    ('Yes, more than double', 'Yes, but less than double', 'No, stable', "I don't know")
)

st.header("2. Data Usage")

access_type = st.radio(
    "What type of access do you need?",
    ('Primarily read', 'Primarily write', 'Balanced read and write', "I don't know")
)

access_frequency = st.radio(
    "How frequently is your data accessed?",
    ('Very frequently (multiple times per second)', 'Frequently (multiple times per minute/hour)', 'Occasionally (a few times per day/week)', 'Rarely (a few times per month/year)', "I don't know")
)

operation_type = st.radio(
    "What operations do you primarily perform on your data?",
    ('Transactions (frequent updates, strong consistency)', 'Analytics (complex queries, aggregations)', 'Archiving (long-term storage)', "I don't know")
)

st.header("3. Technical Requirements")

latency_requirement = st.radio(
    "What is your latency requirement?",
    ('Very low (less than 1 ms)', 'Low (between 1 ms and 10 ms)', 'Medium (between 10 ms and 100 ms)', 'High (more than 100 ms)', "I don't know")
)

scalability_requirement = st.radio(
    "Does your application need to handle a large number of concurrent users?",
    ('Yes, more than 1000', 'Yes, between 100 and 1000', 'No, less than 100', "I don't know")
)

availability_requirement = st.radio(
    "What level of availability is required for your data?",
    ('99.999% (less than 5 minutes of downtime per year)', '99.99% (less than 53 minutes of downtime per year)', '99.9% (less than 8 hours of downtime per year)', 'Less than 99.9%', "I don't know")
)

st.header("4. Other Requirements")

security_requirement = st.multiselect(
    "Do you have specific data security requirements?",
    ['Encryption at rest', 'Encryption in transit', 'Granular access control', 'Access audit', "I don't know"]
)

compliance_requirement = st.radio(
    "Do your data need to comply with specific regulations (GDPR, HIPAA, etc.)?",
    ('Yes', 'No', "I don't know")
)

budget_requirement = st.radio(
    "Do you have a specific budget for storage and database?",
    ('Yes', 'No', "I don't know")
)

st.header("Application Location")

region_deployment = st.radio(
    "Are your applications deployed in:",
    ('A single geographic region', 'Multiple regions within the same continent', 'Multiple continents', "I don't know")
)

replication_need = st.radio(
    "Do you need data replication between different regions for high availability and resilience?",
    ('Yes, synchronous replication (real-time data)', 'Yes, asynchronous replication (slight delay in data)', 'No, replication not needed', "I don't know")
)

st.header("5. Additional Questions")

gcp_integration = st.multiselect(
    "Do you need to integrate your data with other GCP services (BigQuery, AI/ML, etc.)?",
    ['BigQuery', 'AI/ML services', 'Other GCP services', 'No specific integration needed', "I don't know"]
)

sla_requirement = st.radio(
    "Do you have specific SLA requirements for the database?",
    ('Yes, strict SLA (99.999% availability)', 'Yes, medium SLA (99.99% availability)', 'Yes, basic SLA (99.9% availability)', 'No specific requirement', "I don't know")
)

backup_management = st.multiselect(
    "Do you need specific backup and recovery solutions?",
    ['Automatic daily backups', 'Manual weekly backups', 'Point-in-time recovery', 'No specific need', "I don't know"]
)

additional_info = st.text_area("Additional Information")

model_choice = st.selectbox(
    "Choose your Gemini model",
    ("gemini-1.5-pro-001", "gemini-1.5-flash-001")
)

if "response" not in st.session_state:
    st.session_state.response = None
if "user_inputs" not in st.session_state:
    st.session_state.user_inputs = None

# Add an "Analyze" button
if st.button("Analyze"):
    user_inputs = {
        "data_format": data_format,
        "data_volume": data_volume,
        "data_growth": data_growth,
        "access_type": access_type,
        "access_frequency": access_frequency,
        "operation_type": operation_type,
        "latency_requirement": latency_requirement,
        "scalability_requirement": scalability_requirement,
        "availability_requirement": availability_requirement,
        "security_requirement": security_requirement,
        "compliance_requirement": compliance_requirement,
        "budget_requirement": budget_requirement,
        "region_deployment": region_deployment,
        "replication_need": replication_need,
        "gcp_integration": gcp_integration,
        "sla_requirement": sla_requirement,
        "backup_management": backup_management,
        "additional_info": additional_info,
    }
    st.session_state.user_inputs = user_inputs
    
    # Create the prompt for Gemini
    prompt = f"""
    Based on the user's responses to a series of questions about their data needs, please recommend the most suitable Google Cloud Platform (GCP) database solution from the following options. The user’s input includes the type, volume, growth, usage, technical requirements, and additional specifications for their data. Below are the details of each GCP database option:

    1. **Cloud Storage**:
       - Description: Binary/object store
       - Ideal for: Large or rarely accessed unstructured data
       - Not ideal for: Structured data or building fast apps
       - Read/Write Latency: Medium (100s of ms)
       - Typical Size: Any
       - Storage type: Object

    2. **Firestore**:
       - Description: Real-time NoSQL database to store and sync data
       - Ideal for: Mobile, web, multi-user, IoT & real-time applications
       - Not ideal for: Analytic data or heavy writes
       - Read/Write Latency: Medium (10s of ms)
       - Typical Size: Lower than 200 TB
       - Storage type: Document

    3. **Bigtable**:
       - Description: High-volume, low-latency database
       - Ideal for: "Flat," heavy read/write, or analytical data
       - Not ideal for: Highly structured or transactional data
       - Read/Write Latency: Low (ms)
       - Typical Size: 2 TB to 10 PB
       - Storage type: Key-value

    4. **Cloud SQL**:
       - Description: Well-understood VM-based RDBMS
       - Ideal for: Web frameworks and existing applications
       - Not ideal for: Scaling, analytics, or heavy writes
       - Read/Write Latency: Low (ms)
       - Typical Size: Lower than 64 TB
       - Storage type: Relational

    5. **AlloyDB**:
       - Description: Highly scalable PostgreSQL database
       - Ideal for: High performance, availability, and scale
       - Not ideal for: Non-PostgreSQL apps
       - Read/Write Latency: Low (ms)
       - Typical Size: Lower than 64 TB
       - Storage type: Relational

    6. **Spanner**:
       - Description: Global relational database service
       - Ideal for: Low-latency transactional systems
       - Not ideal for: Analytics data
       - Read/Write Latency: Low (ms)
       - Typical Size: Any
       - Storage type: Relational

    7. **BigQuery**:
       - Description: Autoscaling, managed analytic data warehouse
       - Ideal for: Interactive analysis of static datasets
       - Not ideal for: Transactional workloads
       - Read/Write Latency: High (s)
       - Typical Size: Any
       - Storage type: Columnar

     8. **Memorystore**:
       - Description: Fully managed, scalable caching for applications
       - Ideal for: High performance data access
       - Not ideal for: Persistent data
       - Read/Write Latency: Very-Low (sub-ms)
       - Typical Size: 1 TB
       - Storage type: Object

    **User's Input:**
    1. Data Format: {user_inputs['data_format']}
    2. Data Volume: {user_inputs['data_volume']}
    3. Data Growth: {user_inputs['data_growth']}
    4. Access Type: {user_inputs['access_type']}
    5. Access Frequency: {user_inputs['access_frequency']}
    6. Operation Type: {user_inputs['operation_type']}
    7. Latency Requirement: {user_inputs['latency_requirement']}
    8. Scalability Requirement: {user_inputs['scalability_requirement']}
    9. Availability Requirement: {user_inputs['availability_requirement']}
    10. Security Requirement: {user_inputs['security_requirement']}
    11. Compliance Requirement: {user_inputs['compliance_requirement']}
    12. Budget Requirement: {user_inputs['budget_requirement']}
    13. Region Deployment: {user_inputs['region_deployment']}
    14. Replication Need: {user_inputs['replication_need']}
    15. GCP Integration: {user_inputs['gcp_integration']}
    16. SLA Requirement: {user_inputs['sla_requirement']}
    17. Backup Management: {user_inputs['backup_management']}
    18. Additional Information: {user_inputs['additional_info']}

    Please analyze the user's input and recommend the most suitable GCP database solution along with the rationale behind the recommendation.
    """

    # Call Gemini 1.5 Pro for analysis (hypothetical, as actual call requires a specific API)
    with st.spinner('Analyzing...'):
        model = GenerativeModel(model_choice)

        response = model.generate_content(
            prompt,
            generation_config=generation_config,
            safety_settings=safety_settings,
        )
        st.session_state.response = response.text

if st.session_state.response:
    # Display the recommendation
    st.write("Recommended Database Solution:")
    st.markdown(st.session_state.response)

    # Add feedback section
    st.subheader("Feedback")
    feedback = st.radio("Does the recommended solution meet your needs?", ("Yes", "No"))
    
    if feedback == "No":
        feedback_reason = st.text_area("Please specify why the recommendation is not suitable:")

    if st.button("Submit Feedback"):
        # Prepare data for BigQuery
        feedback_data = {
            "user_inputs": json.dumps(st.session_state.user_inputs),
            "response": st.session_state.response,
            "feedback": feedback,
            "reason": feedback_reason if feedback == "No" else "",
        }

        # Insert feedback into BigQuery
        table_id = f"{PROJECT_ID}.{DATASET_ID}.{TABLE_ID}"
        errors = client.insert_rows_json(table_id, [feedback_data])  # Make an API request.

        if errors == []:
            st.write("Thank you for your feedback. It has been recorded.")
        else:
            st.write(f"Encountered errors while inserting rows: {errors}")